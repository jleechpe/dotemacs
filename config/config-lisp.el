;; -*- lexical-binding: t; -*-

;; Configuration specific to Linux systems
(use-package sly
  :straight t
  :if (memq system-type '(gnu/linux))
  :config
  (setq inferior-lisp-program "sbcl")
  (defun sly-connect-stumpwm ()
    (interactive)
    (sly-connect "localhost" "4005")))

(use-package sly-quicklisp
  :straight t
  :after sly
  :if (memq system-type '(gnu/linux)))

(use-package slime
  :disabled t
  :if (memq system-type '(gnu/linux))
  :config
  (load-file "~/quicklisp/slime-helper.el")
  (setq inferior-lisp-program "sbcl"))

;; * Provides
(provide 'config-lisp)
