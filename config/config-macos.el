;; -*- lexical-binding: t; -*-

;; Mac Keyboard setup

(setq mac-pass-command-to-system 't
      ;; Ensure option/alt behaves properly
      mac-option-modifier 'meta
      mac-right-option-modifier 'meta
      ;; Right Win/OS is super, left is hyper if not swallowed by
      ;; operating system
      mac-right-command-modifier 'super
      mac-command-modifier 'hyper)

;; Provides

(provide 'config-macos)
