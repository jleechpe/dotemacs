;; -*- lexical-binding: t; -*-

;; * General Emacs related configurations

;; ** Enable disabled features

(put 'downcase-region 'disabled nil)

;; ** Custom
;; Load a custom file if present, but store it separately from init
;; file.

(use-package cus-edit
  :config
  (setq custom-file (expand-file-name "custom.el"
                                      user-cache-dir))
  (if (file-exists-p custom-file)
      (load custom-file)))

;; ** Backups
;; Keep backups in a centralized location to avoid clutter.

(setq backup-by-copying t
      backup-directory-alist
      `(("." . ,(expand-file-name "backups" user-cache-dir)))
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2)

;; ** Persistence

(use-package savehist
  :init
  (setq savehist-file (expand-file-name "history" user-cache-dir))
  (savehist-mode +1))

(use-package saveplace
  :init
  (setq save-place-forget-unreadable-files t)
  (save-place-mode 1))

(use-package recentf
  :init
  (setq recentf-save-file (expand-file-name "recentf" user-cache-dir))
  (recentf-mode +1))
  
;; ** TODO Verify if still needed
(use-package uniquify
  :disabled t
  :config
  (setq uniquify-buffer-name-style 'forward))

;; ** Buffer view

(defun ibuffer-filter ()
  (ibuffer-projectile-set-filter-groups)
  (unless (eq ibuffer-sorting-mode 'alphabetic)
    (ibuffer-do-sort-by-alphabetic)))
(use-package ibuffer
  :general
  ("C-x C-b" #'ibuffer)
  :config
  (define-ibuffer-column vc-backend
    (:name "VC")
    (format "%s" (or (car (ibuffer-vc-root (current-buffer))) "")))
  (setq ibuffer-formats
        '((mark modified read-only vc-status-mini " "
                (name 18 18 :left :elide)
                " "
                (size 9 -1 :right)
                " "
                (mode 16 16 :left :elide)
                " "
                (vc-backend 8 8 :left)
                " "
                project-relative-file)))
  :hook (ibuffer . ibuffer-filter))

(use-package ibuffer-projectile
  :straight t
  :demand t)
(use-package ibuffer-vc
  :straight t
  :demand t)

;; ** CUA
;; Turn off CUA editing but retain rectangle

(use-package cua-base
  :init (setq cua-enable-cua-keys 'nil))
;; * Provides
(provide 'config-system)
