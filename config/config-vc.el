;; -*- lexical-binding: t; -*-
;; * Version Control
;; ** Git
(use-package magit
  :straight t
  :defer t
  :commands magit-status
  :general
  ("<f12>" #'magit-status))

(use-package closql
  :straight t
  :defer t)

(use-package forge
  :straight t
  :after closql
  :defer t)

;; ** Fossil
(use-package vc-fossil
  :straight t)

;; * Provides
(provide 'config-vc)
