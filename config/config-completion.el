;; -*- lexical-binding: t; -*-

;; * General Completion

;; Enhance default completion framework for minibuffers and any other
;; expansion systems.  

(use-package vertico
  :straight t
  :config
  (vertico-mode))

(use-package vertico-directory
  :load-path "straight/build/vertico/extensions"
  :general
  (:keymaps 'vertico-map
   "\r" #'vertico-directory-enter
   "\d" #'vertico-directory-delete-char
   "M-\d" #'vertico-directory-delete-word)
  )

(use-package orderless
  :straight t
  :init
  (setq completion-styles '(orderless)))

;; Display annotations for commands
(use-package marginalia
  :straight t
  :init (marginalia-mode +1)
  :general
  (minibuffer-mode-map "s-a" #'marginalia-cycle))

;; Enhanced completion commands to allow for selectrum-style
;; completions
(use-package consult
  :straight t
  :config
  (setq consult-project-root-function #'projectile-project-root
        consult-narrow-key "<"
        consult-preview-key '(:debounce 0.2 any))
  :general
  ([remap switch-to-buffer] #'consult-buffer)
  ([remap switch-to-buffer-other-window] #'consult-buffer-other-window)
  ([remap switch-to-buffer-other-frame] #'consult-buffer-other-frame)
  ([remap repeat-complex-command] #'consult-complex-command)
  ([remap goto-line] #'consult-goto-line)
  ("M-g M-g" #'consult-line)
  ([remap apropos-command] #'consult-apropos)
  ([remap yank-pop] #'consult-yank-pop)
  ([remap pop-global-mark] #'consult-global-mark)
  ("C-c k" #'consult-macro)
  ("M-g o" #'consult-outline)
  ("M-g i" #'consult-project-imenu)
  ("M-g M-i" #'consult-imenu)
  ("s-s" #'consult-isearch))

(use-package consult-projectile
  :straight t
  :commands consult-projectile
  :general
  ("C-x f" #'consult-projectile))

;; Custom actions to take based on context
(use-package embark
  :straight t
  :general ("s-e" #'embark-act))

(use-package embark-consult
  :straight t
  :after (consult embark))

;; ** Text autocompletion

;; Leverage company for completion-at-point.  Currently a bit
;; over-zealous in what it completes when typing but this will need
;; some cleanup over time.
(use-package company
  :straight t
  :disabled t
  :hook (after-init . global-company-mode)
  :general
  ("M-/" #'company-complete)
  (:keymaps 'company-active-map
            "C-n" 'company-other-backend)
  :config (setq company-selection-wrap-around t
                company-idle-delay 0
                company-minimum-prefix-length 2
                company-show-numbers t
                ;; company-backends (list 
                ;;                   (list #'company-capf :with :separate
                ;;                         #'company-dabbrev-code
                ;;                         #'company-keywords)
                ;;                   #'company-files
                ;;                   #'company-dabbrev)
                ))

(use-package company-lsp
  :straight t
  :commands company-lsp
  :ensure t
  :disabled t
  :after '(company lsp-mode))

(use-package companty-tabnine
  :straight t
  :ensure t
  :disabled t
  :after '(company))

(use-package corfu
  :straight t
  :hook (after-init . corfu-global-mode)
  :general
  ("M-<tab>" #'complete-symbol)
  ("M-/" #'completion-at-point)
  :config
  (setq corfu-auto t
        corfu-auto-delay 0.1
        corfu-preselect-first t
        corfu-preview-current t
        corfu-cycle t
        corfu-quit-at-boundary nil
        corfu-quit-no-match t
        corfu-scroll-margin 2
        ))

(use-package cape
  :straight t
  :config
  (defun my/tabnine-capf ()
    (interactive)
    (let ((completion-at-point-functions
           (list (cape-company-to-capf #'company-tabnine))))
      (completion-at-point)))
  
  :general
  ("M-<tab>" #'my/tabnine-capf)
  )

(use-package kind-icon
  :straight t
  :after corfu
  :custom
  (kind-icon-default-face 'corfu-default) ; to compute blended backgrounds correctly
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

(use-package company-tabnine
  :straight t
  :ensure t
  :after '(company))

;; ** Snippets

(use-package yasnippet
  :straight t
  :init
  ;; Force snippets into cache rather than root of .emacs.d
  (setq yas-prompt-functions '(yas-completing-prompt
                               yas-dropdown-prompt
                               yas-no-prompt)
        yas-snippet-dirs (list
                          (expand-file-name "snippets" user-cache-dir))))

;; * Provide
(provide 'config-completion)
