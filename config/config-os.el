;; -*- lexical-binding: t; -*-

;; Load specific OS source
 (config-require macos
                :condition (memq system-type '(darwin))
                :fail-silently t)
(config-require windows
                :condition (memq system-type '(windows-nt ms-dos cygwin))
                :fail-silently nil)
(config-require linux
                :condition (memq system-type '(gnu gnu/linux gnu/kfreebsd))
                :fail-silently t)

;; Provide
(provide 'config-os)
