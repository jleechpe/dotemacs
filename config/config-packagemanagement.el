;; -*- lexical-binding: t; -*-

;; Ensure straight was loaded

(unless (featurep 'straight)
  (load-file (expand-file-name "early-init.el" user-emacs-directory)))

;; * Initialize package management (Use Package)
(straight-use-package 'use-package)

(use-package use-package
  :init
  (setq use-package-verbose t
        use-package-compute-statistics t))

(use-package general
  :straight t
  :demand t)

(use-package use-package-chords
  :straight t
  :disabled t
  :config (key-chord-mode 1))

;; * Benchmarking init file

(cl-letf (((symbol-function 'define-obsolete-function-alias) #'defalias))
  (use-package benchmark-init
    :straight t
    :ensure t
    :config
    (progn
      (require 'benchmark-init-modes)   ; explicitly required
      (add-hook 'after-init-hook #'benchmark-init/deactivate))))

;; Load newest version of packages
(setq load-prefer-newer t)

;; * Provides
(provide 'config-packagemanagement)
