;; -*- lexical-binding: t; -*-

;; * UI Defaults
(fset 'yes-or-no-p 'y-or-n-p)
(tool-bar-mode -1)
(menu-bar-mode 1)
(column-number-mode 1)

(use-package whitespace
  :init
  (setq whitespace-line-column 80
        whitespace-global-modes '(not circe-mode)
        whitespace-style '(tabs newline tab-mark space-mark
                                newline-mark face lines-tail)
        whitespace-display-mappings '(
                                      (space-mark nil)
                                      (newline-mark 10 [172 10])
                                      (tab-mark 9 [183 9] [92 9])))
  :config (global-whitespace-mode 1))

;; ** Font
(setq emacs-font "Fira Code Nerd Font")
(set-fontset-font t nil "Noto Color Emoji")
; (setq emacs-font "MesloLGS NF")

(set-face-attribute 'default nil
                    :family emacs-font
                    :height 110
                    :weight 'normal
                    :width 'normal)

;; ** Theme
;; Include rainbow mode since it is for coloring of text
(use-package rainbow-mode
  :straight t
  :hook
  (prog-mode . rainbow-mode))

;; Solaire mode makes non-file buffers slightly different background
;; for visibility
(use-package solaire-mode
  :straight t
  :init
  (setq solaire-mode-auto-swap-bg 't)
  :hook (after-init . solaire-global-mode))

(use-package doom-themes
  :init
  :config
  (load-theme 'doom-spacegrey t)
  (defun theme-color (color)
    (nth 2 (assoc color doom-themes--colors)))
  :ensure t
  :demand t
  :straight t)

;; ** Modeline

(use-package doom-modeline
  :init (doom-modeline-mode +1)
  :after solaire-mode
  :config
  (setq doom-modeline-minor-modes 't
        doom-modeline-checker-simple-format 't
        ; TODO Check other formats as I go
        doom-modeline-buffer-file-name-style 'truncate-with-project
        doom-modeline-enable-word-count 't
        doom-modeline-hud 't
        doom-modeline-icon 't
        ; TODO See what other modes as I go
        doom-modeline-continuous-word-count-modes '(markdown-mode org-mode)
        doom-modeline-indent-info 't
        doom-modeline-hud 't
        ; Will be useful once migrated to github
        doom-modeline-github 't
        doom-modeline-env-version 't
        doom-modeline-env-load-string "?env?")
  :straight t)

(use-package minions
  :init (minions-mode +1)
  :straight t)

;; ** Delimiters
(use-package rainbow-delimiters
  :straight t
  :hook (prog-mode . rainbow-delimiters-mode)
  :init
  (show-paren-mode 1))

;; * File treeview
;; Treemacs allows management of projects/workspaces and filtering by
;; perspective to show desired folders rather than purely follow the
;; current file.

;; ** TODO Add keybindings
(use-package treemacs
  :general
  ("M-0" #'treemacs-select-window)
  (:prefix "s-t"
           "1"   #'delete-other-windows
           "t"   #'treemacs
           "B"   #'treemacs-bookmark
           "C-t" #'treemacs-find-file
           "M-t" #'treemacs-find-tag)
  :config
  (treemacs-fringe-indicator-mode 'only-when-focused)
  (defun aorst/treemacs-setup-title ()
    (let* ((bg (face-attribute 'default :background))
           (bg2 (doom-lighten bg 0.2))
           (fg (face-attribute 'default :foreground)))
      (face-remap-add-relative 'header-line
                               :background bg :foreground fg
                               :box `(:line-width ,(/ (line-pixel-height) 4) :color ,bg2)))
    (setq header-line-format
          '((:eval
             (let* ((text (treemacs-workspace->name (treemacs-current-workspace)))
                    (extra-align (+ (/ (length text) 2) 0))
                    (width (floor (- (/ (window-width) 2) extra-align))))
               (concat (make-string width ?\s) text))))))
  :hook (treemacs-mode . aorst/treemacs-setup-title)
  :straight t)

(use-package treemacs-magit
  :straight t
  :ensure t
  :after (treemacs magit))

;; * Provide
(provide 'config-ui)
