;; -*- lexical-binding: t; -*-

;; Windoows Keyboard setup

(progn
  (setq-default buffer-file-coding-system 'utf-8-unix)
  (setq ispell-local-dictionary "en_US")
  (setq ispell-local-dictionary-alist
        '(("en_US" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil ("-d" "en_US") nil utf-8)))
  (setq ispell-hunspell-dictionary-alist ispell-local-dictionary-alist)
  (setq w32-lwindow-modifier 'hyper
        w32-rwindow-modifier 'super)
  (w32-register-hot-key [s-])
  (w32-register-hot-key [H-]))

;; Provides

(provide 'config-windows)

