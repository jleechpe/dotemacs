;; -*- lexical-binding: t; -*-

;; * Defaults
(setq-default indent-tabs-mode nil)
(setq require-final-newline t)

;; * Specific functionality

;; ** Keybindings

(use-package which-key
  :straight t
  :init
  (which-key-mode +1))

(general-def
  "C-M-i" #'delete-indentation
  "C-M-=" #'align-regexp)
;; ** Diffs

(use-package ediff
  :defer t
  :init
  (setq ediff-window-setup-function #'ediff-setup-windows-plain
        ediff-split-window-function #'split-window-horizontally))

;; ** Improve search info

(use-package anzu
  :init (global-anzu-mode +1)
  :general
  ([remap query-replace] #'anzu-query-replace)
  ([remap query-replace-regexp] #'anzu-query-replace-regexp)
  :straight t)

;; ** Undo

(use-package undo-tree
  :straight t
  :diminish
  :config (global-undo-tree-mode 1))

;; ** SmartParens

(use-package smartparens
  :straight t
  :hook (prog-mode . smartparens-mode))
(use-package smartparens-config)

;; ** Dired improvements

(use-package sunrise-commander
  :straight t
  :defer t
  :commands sunrise)

;; ** TODO Multiple Cursors
;; Needs keybindings

(use-package multiple-cursors
  :straight t
  :general
  ("C-<" #'mc/mark-previous-like-this)
  ("C->" #'mc/mark-next-like-this)
  ("C-c C-<" #'mc/mark-all-like-this-dwim))

;; ** Project Configuration
(use-package projectile
  :straight t
  :init
  (setq projectile-mode-line-prefix " Prj"
        projectile-completion-system 'auto
        projectile-globally-ignored-files '(".tfstate" ".gitignore"))
  (add-to-list 'projectile-globally-ignored-directories ".terraform")
  (projectile-mode 1)
  :general
  ("C-c p" #'projectile-command-map))

;; ** Window management
(use-package ace-window
  :straight t
  :general
  ("M-o" #'ace-window)
  :init
  (ace-window-display-mode +1))

;; * Provide

(provide 'config-ux)
