;; -*- lexical-binding: t; -*-

;; * TODO This only works with gpg based ssh-agent, may need
;; * refactoring
(if (memq system-type '(gnu/linux))
    (setenv "SSH_AUTH_SOCK" "/run/user/1000/gnupg/S.gpg-agent.ssh"))



;; Provide
(provide 'config-linux)
