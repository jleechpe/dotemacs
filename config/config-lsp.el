;; -*- lexical-binding: t; -*-

(use-package flycheck
  :straight t
  :defer t
  :hook ((yaml-mode powershell-mode) . flycheck-mode))

(use-package lsp-ui
  :straight t
  :after lsp-mode
  :config
  (setq lsp-ui-sideline-show-diagnostics t
        lsp-ui-sideline-show-hover t
        lsp-ui-sideline-show-code-actions t
        lsp-ui-sideline-show-symbol t
        lsp-ui-doc-enable nil
        lsp-modeline-code-actions-enable nil
        lsp-modeline-diagnostics-enable nil)
  :general
  (lsp-ui-mode-map
            [remap xref-find-definitions] #'lsp-ui-peek-find-definitions
            [remap xref-find-references] #'lsp-ui-peek-find-references)
  (lsp-command-map
   "s-m" #'lsp-ui-imenu)
  :commands lsp-ui-mode)

(use-package lsp-mode
  :straight t
  :defer t
  :commands lsp
  :init
  (setq lsp-prefer-flymake nil)
  (if (memq system-type '(windows-nt))
      (setq lsp-keymap-prefix "s-o"))
  (defun my/lsp-mode-setup-completion ()
    (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
          '(orderless)))
  ;; Use Terraform-ls
  :config
  (setq lsp-completion-provider :none)
  
  :hook (((powershell-mode
          yaml-mode
          dockerfile-mode
          terraform-mode
          ;; markdown-mode ; Leave markdown as manual until spellcheck resolved
          vue-mode) . lsp-deferred)
         (lsp-mode . yas-minor-mode)
         (lsp-completion-mode . my/lsp-mode-setup-completion)
         ))

;; * Provides

(provide 'config-lsp)
