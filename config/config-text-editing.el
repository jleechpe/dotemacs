;; -*- lexical-binding: t; -*-

(use-package flyspell-correct
  :straight t
  :general
  ([remap flyspell-auto-correct-word] #'flyspell-correct-wrapper))

(use-package markdown-mode
  :straight t
  :hook ((markdown-mode . auto-fill-mode)
         (markdown-mode . flyspell-mode)
         (markdown-mode . flycheck-mode)))

;; * Provides
(provide 'config-text-editing)
