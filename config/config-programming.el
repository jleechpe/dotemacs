;; -*- lexical-binding: t; -*-

;; * Programming related

;; ** Programming language modes

(use-package terraform-mode
  :straight t
  :hook ((terraform-mode . terraform-format-on-save-mode)))

(use-package fsharp-mode
  :straight t
  :defer t)

(use-package powershell
  :straight t
  :defer t)

(use-package puppet-mode
  :straight t
  :defer t)

(use-package dockerfile-mode
  :straight t
  :defer t)

;; *** Python
(use-package python
  :config
  (setq python-indent-guess-indent-offset-verbose nil)
  (cond
   ((executable-find "ipython")
    (progn
      (setq python-shell-buffer-name "IPython"
            python-shell-interpreter "ipython"
            python-shell-interpreter-args "-i --simple-prompt")))
   ((executable-find "python3")
    (setq python-shell-interpreter "python3"))
   ((executable-find "python2")
    (setq python-shell-interpreter "python2"))
   (t
    (setq python-shell-interpreter "python"))))

(use-package dap-mode
  :straight t
  :defer t
  :after lsp-mode
  :config
  (dap-auto-configure-mode))

(use-package inferior-python-mode
  :ensure nil
  :hook (inferior-python-mode . hide-mode-line-mode))

(use-package lsp-pyright
  :defer t
  :straight t
  :config
  (setq lsp-pyright-disable-language-services nil
        lsp-pyright-disable-organize-imports nil
        lsp-pyright-auto-import-completions t
        lsp-pyright-use-library-code-for-types t)
  :hook ((python-mode . (lambda ()
                          (require 'lsp-pyright) (lsp-deferred)))))

(use-package blacken
  :straight t
  :defer t
  :after python
  :hook (python-mode . blacken-mode))

;; ** Data
(use-package yaml-mode
  :straight t
  :defer t)

(use-package json-mode
  :straight t
  :defer t)

;; ** OS Packages
(use-package systemd
  :straight t
  :defer t)

;; * Provides
(provide 'config-programming)
