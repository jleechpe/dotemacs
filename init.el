;; -*- lexical-binding: t; -*-

;; 2021 startup

;; Add config directory to load path.  All files should start with
;; ~config-~ to provide a consistent naming
(add-to-list 'load-path (expand-file-name "config" user-emacs-directory))
(straight-use-package 'org)
;; Setup variables and load macros
(require 'config-init)
;; Always enable package management
(config-require packagemanagement :condition t :silent nil)

;; Require OS Specific lib
(config-require os :condition t :silent nil)

(config-packages '(system
                   vc
                   server
                   ui
                   ux
                   completion
                   lsp
                   programming
                   text-editing
                   lisp
                   ;; mail
                   org-mode))
(if (memq system-type '(gnu/linux))
    (config-packages '(mail)))

;; * Introduction

;; * Global configuration

(use-package explain-pause-mode
  :straight t
  :disabled t
  :init
  (explain-pause-mode))

;; ** UI
;; General defaults


;; *** Font
;; (let ((alist '((33 . ".\\(?:\\(?:==\\|!!\\)\\|[!=]\\)")
;;                (35 . ".\\(?:###\\|##\\|_(\\|[#(?[_{]\\)")
;;                (36 . ".\\(?:>\\)")
;;                (37 . ".\\(?:\\(?:%%\\)\\|%\\)")
;;                (38 . ".\\(?:\\(?:&&\\)\\|&\\)")
;;                ;; (42 . ".\\(?:\\(?:\\*\\*/\\)\\|\\(?:\\*[*/]\\)\\|[*/>]\\)")
;;                (43 . ".\\(?:\\(?:\\+\\+\\)\\|[+>]\\)")
;;                (45 . ".\\(?:\\(?:-[>-]\\|<<\\|>>\\)\\|[<>}~-]\\)")
;;                (46 . ".\\(?:\\(?:\\.[.<]\\)\\|[.=-]\\)")
;;                (47 . ".\\(?:\\(?:\\*\\*\\|//\\|==\\)\\|[*/=>]\\)")
;;                (48 . ".\\(?:x[a-zA-Z]\\)")
;;                (58 . ".\\(?:::\\|[:=]\\)")
;;                (59 . ".\\(?:;;\\|;\\)")
;;                (60 . ".\\(?:\\(?:!--\\)\\|\\(?:~~\\|->\\|\\$>\\|\\*>\\|\\+>\\|--\\|<[<=-]\\|=[<=>]\\||>\\)\\|[*$+~/<=>|-]\\)")
;;                (61 . ".\\(?:\\(?:/=\\|:=\\|<<\\|=[=>]\\|>>\\)\\|[<=>~]\\)")
;;                (62 . ".\\(?:\\(?:=>\\|>[=>-]\\)\\|[=>-]\\)")
;;                (63 . ".\\(?:\\(\\?\\?\\)\\|[:=?]\\)")
;;                (91 . ".\\(?:]\\)")
;;                (92 . ".\\(?:\\(?:\\\\\\\\\\)\\|\\\\\\)")
;;                (94 . ".\\(?:=\\)")
;;                (119 . ".\\(?:ww\\)")
;;                (123 . ".\\(?:-\\)")
;;                (124 . ".\\(?:\\(?:|[=|]\\)\\|[=>|]\\)")
;;                (126 . ".\\(?:~>\\|~~\\|[>=@~-]\\)"))))
;;   (dolist (char-regexp alist)
;;     (set-char-table-range composition-function-table (car char-regexp)
;;                           `([,(cdr char-regexp) 0 font-shape-gstring]))))


;; *** Shell
;; Turn off auto-composition when using shell due to interpolation
;; (use-package eshell
;;   :hook (eshell-mode . (lambda () (auto-composition-mode -1))))


;; *** Project configuration


;; ** Email
;; Currently not in use while identifying how to get emails to flag
;; properly


;; * OS-split configuration
(setq kbfs-dir (if (memq system-type '(gnu-linux))
                   "~/keybase"
                 "k:/")
      kbfs-org (expand-file-name "private/jleechpe/work" kbfs-dir))

;; * COMMENT Configuration

;; Local Variables:
;; outorg-export-export-commands: (("Introduction" md "README"))
;; End:
