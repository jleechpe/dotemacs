;; This buffer is for text that is not saved, and for Lisp evaluation.
;; To create a file, visit it with C-x C-f and enter text in its buffer.

(nth 2 (assoc 'methods doom-themes--colors))


(mapc (lambda (x) (straight-use-package x))'(embark selectrum marginalia consult orderless prescient company-prescient selectrum-prescient yasnippet))







(use-package persp-mode
  :straight t)

(use-package treemacs-persp
  :straight t)

(use-package persp-projectile
  :straight t)

(provide 'testconfig)
