(setq native-comp-deferred-compilation-deny-list '())

(setq comp-num-cpus 2
      comp-deferred-compilation-deny-list '())

;; Prevent package.el from loading packages and enable straight.el
(setq package-enable-at-startup 'nil
      straight-enable-package-integration 'nil
      straight-recipes-gnu-elpa-use-mirror t)
;; Ensure straight is downloaded and bootstrapped
(let (;; (straight-base-dir (expand-file-name "cache" user-emacs-directory))
      (bootstrap-file(concat
                      user-emacs-directory
                      "straight/repos/straight.el/bootstrap.el"))
      (bootstrap-version 3))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'no-message))

(provide 'early-init)
